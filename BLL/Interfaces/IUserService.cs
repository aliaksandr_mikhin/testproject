﻿using BLL.Entities;
using System.Collections.Generic;
 

namespace BLL.Interfaces.Services
{
    public interface IUserService
    {
        IEnumerable<UserEntity> GetAllUserEntities();
        void CreateUser(UserEntity user);
    }
}