﻿using System;
using System.Linq;
using BLL.Interfaces.Services;
using DependencyResolver;
using Ninject;

namespace ConsolePL
{
	class Program
	{
		static void Main(string[] args)
		{
			RevolverModule module = new RevolverModule();
			IKernel kernel = new StandardKernel(module);
			var service = kernel.Get<IUserService>();
			var list = service.GetAllUserEntities().ToList();
			foreach (var user in list)
			{
				Console.WriteLine(user.UserName);
			}
		}
	}
}
