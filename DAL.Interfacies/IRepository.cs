﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.Interfaces
{
    public interface IRepository<TEntity, TKey> where TEntity : IEntity<TKey>
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(TKey key);
        TEntity GetByPredicate(Expression<Func<TEntity, bool>> f);
        void Create(TEntity e);
        void Delete(TEntity e);
        void Update(TEntity entity);
    }
}