﻿using DAL.DTO;
using DAL.Interfaces;

namespace DAL.Concrete
{
    public interface IUserRepository : IRepository<DalUser, int>
    {
    }
}