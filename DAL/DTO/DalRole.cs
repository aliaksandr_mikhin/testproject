﻿using DAL.Interfaces;

namespace DAL.DTO
{
    public class DalRole : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
