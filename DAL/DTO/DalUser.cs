﻿using DAL.Interfaces;

namespace DAL.DTO
{
    public class DalUser : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
    }
}